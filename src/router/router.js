import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/search',
      children: [
        {
          path: 'search',
          name: 'om-search',
          component: () => import('@/views/Search')
        },
        {
          path: 'results',
          name: 'om-results',
          component: () => import('@/views/Results/Results')
        },
        {
          path: 'about',
          name: 'om-about',
          component: () => import('@/views/About/About')
        }
      ]
    },
  ],
})

router.beforeEach((to, from, next) => {
  const path = to.path
  store.dispatch('clearError')
  console.log('to', to)
  if (path == "/results" && store.search != true) next('/search')
  next()
})

export default router