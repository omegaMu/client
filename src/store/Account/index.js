import web3 from '../../bootstrap'

export default {
    state: {
        currentAddress: null
    },
    mutations: {
        setAddress(state, payload) {
            state.currentAddress = payload
        }
    },
    actions: {
        getAccount({ commit, getters }) {

            web3.eth.getAccounts((error, accounts) => {
                if (accounts[0]) {
                    commit('setAddress', accounts[0])
                }
                else commit('setAddress', null)
                // else this.dispatch('setListener')
            })
        },
        setAccountStatusListener({ commit, getters }) {
            web3.currentProvider.publicConfigStore.on('update', () => {
                this.dispatch('getAccount')
            })
        },
        getAccountUpdate({ commit, getters }, payload) {
            // web3.eth.getAccounts((error, accounts) => {
            //     console.log(accounts[0])
            //     if (accounts[0]) commit('setAddress', accounts[0])
            //     else this.dispatch('setListener')
            // })
        },
    },
    getters: {
        currentAddress(state) {
            return state.currentAddress
        }
    }
}