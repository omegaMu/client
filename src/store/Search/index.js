import axios from 'axios'

// const base = 'https://j3s5npqtsh.execute-api.us-east-1.amazonaws.com/dev/' //dev version
const base = 'https://w2qvfw2n77.execute-api.us-east-1.amazonaws.com/prod/' //prod version
// const base = 'http://localhost:3000/'
export default {
  state: {
    score: null,
    loading: false,
    search: false,
    error: false,
    searchedAddress: null
  },
  mutations: {
    setScore(state, payload) {
      //   const x = Math.floor(Math.random() * 100)
      state.score = payload
    },
    setLoading(state, payload) {
      state.loading = payload
    },
    setSearch(state, payload) {
      state.search = payload
    },
    setSearchedAddress(state, payload) {
      state.searchedAddress = payload
    },
    setError(state, payload) {
      console.log('errorrr')
      state.error = payload
    }
  },
  actions: {
    // getScore({ commit, getters }, payload) {
    //   commit('setScore', null)
    //   console.log('commit', payload, this.getters.score)
    //   commit('setLoading', true)
    //   commit('setSearch', true)
    //   axios.get(`${base}getscore/${payload}`, {
    //     'Access-Control-Allow-Origin': '*',
    //   })
    //     .then(res => {
    //       console.log('res', res.data.score)
    //       commit('setScore', res.data.score)
    //       commit('setLoading', false)
    //     })
    //     .catch(err => {
    //       console.log(err)
    //       commit('setError', true)
    //       commit('setLoading', false)
    //     })

    // },
    getScore({ commit, getters }) {
      const address = this.getters.currentAddress
      commit('setSearchedAddress', address)
      commit('setScore', null)
      commit('setLoading', true)
      commit('setSearch', true)

      axios.get(`${base}getscore/${address}`, {
        headers: {
          'x-api-key': 'publicApiKeyForBetaTesting'
        }
      })
        .then(res => {
          console.log('positive factor score', res.data.score)
          commit('setScore', res.data.score)
          commit('setLoading', false)
        })
        .catch(err => {
          console.log('err', err)
          commit('setError', true)
          commit('setLoading', false)
        })

    },
    getScoreWithSearch({ commit, getters }, payload) {
      const address = payload
      commit('setSearchedAddress', address)
      commit('setScore', null)
      commit('setLoading', true)
      commit('setSearch', true)

      axios.get(`${base}getscore/${address}`, {
        headers: {
          'x-api-key': 'publicApiKeyForBetaTesting'
        }
      })
        .then(res => {
          commit('setScore', res.data.score)
          commit('setLoading', false)
        })
        .catch(err => {
          console.log(err)
          commit('setError', true)
          commit('setLoading', false)
        })

    },
    clearError({ commit, getters }) {
      commit('setError', false)
    }
  },
  getters: {
    score(state) {
      return state.score
    },
    loading(state) {
      return state.loading
    },
    search(state) {
      return state.search
    },
    error(state) {
      console.log('errrrrrr', state.error)
      return state.error
    },
    searchedAddress(state) {
      return state.searchedAddress
    }
  }
}
