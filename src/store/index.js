import Vue from 'vue'
import Vuex from 'vuex'
import search from './Search'
import account from './Account'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    search,
    account
  }
})
